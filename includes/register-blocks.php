<?php

function my_gutenberg_register_blocks(){
    $asset_file = include (plugin_dir_path(__FILE__) ."../build/index.asset.php");

    wp-enqueue-script(
        "my-block-editor",
        plugins_url("build/index.js", __DIR__.'/../'),
        $asset_file['dependencies'],
        $asset_file['version']
    );
    
    wp-register-style(
        "my-block-editor-style",
        plugins_url("build/editor.css", __DIR__.'/../'),
        array(),
        $asset_file['version']
    );
    
    wp-register-style(
        "my-block-style",
        plugins_url("build/style.css", __DIR__.'/../'),
        array(),
        $asset_file['version']
    );

    require_once plugin_dir_path( __FILE__ ).'blocks/sketchfab-block.php';
    //require_once plugin_dir_path( __FILE__ ).'includes/register-block.php';
}
add_action('init', 'my_guntenberg_blocks_register_blocks');